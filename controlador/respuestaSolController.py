from app import app
from modelo.tipoSolicitud import *
from modelo.solicitud import *
from modelo.oficina import  *
from modelo.respuestaSolicitud import *


from flask import Flask, request, render_template,jsonify,session
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import exc


@app.route("/consultaCodigo",methods=['POST','GET'])
def consultaCodigo():

    datos=None
    estado=False
    codigo = request.form['txtCodigo']
    print(codigo)
    try:
        consulta = ResSolicitud.query.join(Solicitud).join(TipoSolicitud).filter(Solicitud.solCodigo == codigo).first()
        print(consulta)
        if(consulta!=None):
            fechaRespuesta = consulta.resFechaRespuesta
            nuevaFechaRespuesta = fechaRespuesta.strftime("%Y-%m-%d") 
            datos=(consulta.idRespuestaSol ,consulta.solicitud.tipoSolicitud.tipoSolicitud, consulta.resDescripcion,nuevaFechaRespuesta)
            estado=True
            mensaje="Datos del consulta por codigo"
            print(datos)
        else:
            mensaje="No existe consulta por el codigo diligenciado"
    except exc.SQLAlchemyError as ex:
        mensaje = str(ex)
    
    # infoRes = ResSolicitud.query.all()
    # print(infoRes)
    return jsonify({"estado":estado, "datos":datos, "mensaje":mensaje})


# @app.route('/consultaCodigo2')
# def conCodigo():   

#     return render_template("acercaNosotros.html")