from app import app
from re import X
import os
from modelo.tipoSolicitud import *
from modelo.solicitud import *
from modelo.oficina import  *
from modelo.respuestaSolicitud import *
from werkzeug.utils import secure_filename
from typing import Container
from flask import  Flask, request, render_template,jsonify, redirect, url_for, flash, session
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import exc
import yagmail

@app.route('/agregarQueja', methods=['POST','GET'])
def cliente():
    tipoEnvio = request.form['txtipoEnvio']
    print(tipoEnvio)
    if(int(tipoEnvio) == 1):
        nombre = "null"
        correo = "null"
    if(int(tipoEnvio) == 2):
        nombre = request.form['txtNombre']
        correo = request.form['txtCorreoSolicitud']
    tipo = request.form['txtTipoSolicitud']
    descripcion = request.form['txtDescripcion']
    oficina = request.form['txtOficina']
    estado = False
    datos = None
    if(tipo and descripcion and oficina):
        try:
            print(tipo)
            numero = 100
            identificar = format(id(numero),'x')
            solicitud = Solicitud()
            solicitud.idTipoSolicitud = tipo
            solicitud.idOficina = oficina
            solicitud.solDescripcion = descripcion
            solicitud.solNombreCiudadano = nombre
            solicitud.solCorreoCiudadano = correo
            solicitud.solCodigo = identificar
            solicitud.solFechaRegistro = datetime.now()
            db.session.add(solicitud)
            db.session.commit()
            """
            enviar al correo
            """
            mensaje =f"saludos  {nombre} {correo} le informamos que sucodigo es {identificar}"
            asunto = "Registrado en contactos"
            try:
                print("entrooo")
                email = yagmail.SMTP("3222272740ny@gmail.com",open("password").read())
                email.send(to=correo, subject=asunto, contents=mensaje)
            except:
                print("correo no enviado")
            """
            subiendo archivo
            """
            f = request.files['txtdocu']
            print(f)
            filename = secure_filename(f.filename)
            extension =filename.rsplit('.',1)[1].lower()
            nuevoNombre = str(solicitud.idSolicitud) + "." + extension
            f.save(os.path.join(app.config['UPLOAD_FOLDER'], nuevoNombre))
            datos= solicitud.idSolicitud
            estado = True
            mensaje = "PQR registrado correctamente"
        except exc.SQLAlchemyError as ex:
            mensaje = str(ex)
    else:
        mensaje = "Faltan datos"
    return jsonify({"estado":estado, "datos":datos, "mensaje":mensaje})

@app.route("/listarSolicitudes")
def listarSolicitudes():
    userOficina = 1
    solicitudes = Solicitud.query.filter(Solicitud.idOficina == userOficina).all()
    return render_template("empleado/frmSolicitudes.html",  solicitudes=solicitudes)

@app.route("/llenarIdSoli", methods=['POST'])
def llenarCampoId():
    idSoli = int(request.form['idSolicitud'])
    estado = False
    sol = Solicitud.query.filter(Solicitud.idSolicitud == idSoli).first()
    if(sol!=None):
        datos=(sol.idSolicitud, sol.tipoSolicitud.tipoSolicitud, sol.solDescripcion)
        estado = True
        mensaje = "Datos de la solicitud"
    else:
        mensaje = "No existe solicitud con este correo"
    return jsonify({"estado":estado, "datos":datos, "mensaje":mensaje})

@app.route('/agregarRespuesta', methods=['POST'])
def agregarRespuesta():
    idSoli = request.form['idSolicitud']
    print(idSoli)
    respuesta = request.form['txtRespuesta']
    print("llegué al controlller")
    estado = False
    datos = None
    if(idSoli and respuesta):
        try:
            respuestaSol = ResSolicitud()
            respuestaSol.idSolicitud = idSoli
            respuestaSol.resFechaRespuesta = datetime.now()
            respuestaSol.resDescripcion = respuesta
            db.session.add(respuestaSol)
            db.session.commit()
            f = request.files['txtImagen']
            print(f)
            filename = secure_filename(f.filename)
            extension =filename.rsplit('.',1)[1].lower()
            nuevoNombre = str(respuestaSol.idRespuestaSol) + "." + extension
            f.save(os.path.join(app.config['UPLOAD_FOLDER'], nuevoNombre))
            datos= respuestaSol.idRespuestaSol
            estado = True
            mensaje = "Respuesta de PQR registrado correctamente"
            
        except exc.SQLAlchemyError as ex:
            mensaje = str(ex)
    else:
        mensaje = "Faltan datos"
    return jsonify({"estado":estado, "datos":datos, "mensaje":mensaje})
