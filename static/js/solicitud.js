$(function () {
  $("#btnEnviarSolicitud").click(function () {
    agregarQueja();
  });

  $(".editar").click(function () {
    idSolicitud = $(this).attr("data-id");
    llenarCampos(idSolicitud);
  });

  $("#btnEnviaRespuesta").click(function () {
    agregarRespuesta();
  });

  function agregarQueja() {
    const formQueja = new FormData($("#frmModalSolicitud")[0]);

    $.ajax({
      url: "/agregarQueja",
      data: formQueja,
      type: "post",
      dataType: "json",
      cache: false,
      contentType: false,
      processData: false,
      success: function (resultado) {
        console.log(resultado);
        if (resultado.estado) {
          swal(
            "¡Correcto!!",
            resultado.mensaje +
              " En 20 minutos llegará su pedido a la dirección registrada.",
            "success"
          );
          $("#modalpqr").modal("hide");
        }
      },
      error: function (resultado) {
        console.log(resultado);
        swal("Problemas!!", resultado.mensaje, "error");
      },
    });
  }

  function llenarCampos(idSolicitud) {
    var parametros = {
      idSolicitud: idSolicitud,
    };
    $.ajax({
      url: "/llenarIdSoli",
      data: parametros,
      type: "post",
      dataType: "json",
      cache: false,
      success: function (resultado) {
        console.log(resultado);
        if (resultado.estado) {
          $("#idSolicitud").val(resultado.datos[0]);
          $("#txtTipoSolicitud").val(resultado.datos[1]);
          $("#txtDescripcion").val(resultado.datos[2]);
          $("#modalRespuestaSol").modal();
        } else {
          $("#mensaje").html(resultado.mensaje);
        }
      },
      error: function (resultado) {
        console.log(resultado);
      },
    });
  }

  function agregarRespuesta() {
    const formQueja = new FormData($("#frmModalRespuesta")[0]);
    $.ajax({
      url: "/agregarRespuesta",
      data: formQueja,
      type: "post",
      dataType: "json",
      cache: false,
      contentType: false,
      processData: false,
      success: function (resultado) {
        console.log(resultado);
        if (resultado.estado) {
          $("#modalRespuestaSol").modal("hide");
        }
      },
      error: function (resultado) {
        console.log(resultado);
      },
    });
  }
});
