$(function(){

    $("#btnConsultarRes").click(function(){
        if(($("#txtCodigoRespuesta").val()!="")){
            consultarResSolicitud();
            
            
        }else{
            alertify.warning('Llenar el campo del código');
        }
  
    });
});


function consultarResSolicitud(){
    // $("#datosCliente").html("");
    var parametros = {        
        txtCodigo: $("#txtCodigoRespuesta").val()
    };
    console.log(parametros);
    $.ajax({
        url: "/consultaCodigo",
        data: parametros,
        type: "post",
        dataType: "json",
        cache: false,
        success: function(resultado){
            console.log(resultado);
            if(resultado.estado){
                $("#txtFechaRespuesta").val(resultado.datos[3]);
                $("#txtTipoSol").val(resultado.datos[1]);
                $("#txtDescripcion").val(resultado.datos[2]);
                $("#verPDF").attr("href","static/archivos/"+resultado.datos[0]+".pdf");
                $("#modalSolicitud").modal();

                $("#txtCodigoRespuesta").val('');
            }else{
                alertify.warning('La solicitud aún no tiene respuesta')
                $("#txtCodigoRespuesta").val('');
            }
        },
        error: function(resultado){
            console.log(resultado);
        }
    });
}