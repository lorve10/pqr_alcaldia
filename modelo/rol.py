from app import db


class Rol(db.Model):
    __tablename__ ="rol"
    idRol = db.Column(db.Integer, primary_key=True, autoincrement=True)
    rolNombre = db.Column(db.String(30), nullable=False)

    def __repr__(self):
        return f'{self.idRol}'
