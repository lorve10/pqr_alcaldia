from app import db

class Empleado(db.Model):
    __tablename__ = 'empleados' #Nombre de la tabla
    idEmpleado = db.Column(db.Integer, primary_key=True, autoincrement=True)
    idPersona = db.Column(db.Integer, db.ForeignKey('personas.idPersona'), nullable=False)
    idOficina = db.Column(db.Integer, db.ForeignKey('oficinas.idOficina'), nullable=False)
    # Necesarios para la relaicion
    persona = db.relationship("Persona",backref=db.backref('personas',lazy=True))
    oficina = db.relationship("Oficina",backref=db.backref('oficinas',lazy=True))

    def __repr__(self):
        return f'{self.idPersona}'
