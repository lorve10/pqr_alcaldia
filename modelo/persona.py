from app import db

class Persona(db.Model):
    __tablename__ = 'personas' #Nombre de la tabla
    idPersona = db.Column(db.Integer, primary_key=True, autoincrement=True)
    perDocumento = db.Column(db.Integer, nullable=False)
    perNombres = db.Column(db.String(50),nullable=False)
    perApellidos = db.Column(db.String(50),nullable=False)
    perCorreo = db.Column(db.String(50),nullable=False)
    perGenero = db.Column(db.String(20), nullable=False)

    def __repr__(self):
        return f'{self.perNombres}'
