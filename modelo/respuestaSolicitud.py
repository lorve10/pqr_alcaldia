from app import db
from datetime import datetime


class ResSolicitud(db.Model):
    __tablename__ = 'respuestasolicitud' #Nombre de la tabla
    idRespuestaSol = db.Column(db.Integer, primary_key=True, autoincrement=True)
    idSolicitud = db.Column(db.Integer, db.ForeignKey('solicitudes.idSolicitud'), nullable=False)
    resFechaRespuesta = db.Column(db.DateTime, default=datetime.now(), nullable=False)
    resDescripcion = db.Column(db.String(300), nullable=False)
    # Necesarios para la relacion
    solicitud = db.relationship("Solicitud",backref=db.backref('solicitudes',lazy=True))

    def __repr__(self):
        return f'{self.idRespuestaSol}, {self.resFechaRespuesta}, {self.resDescripcion}, {self.idSolicitud}'
