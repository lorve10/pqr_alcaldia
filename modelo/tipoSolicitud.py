from app import db

class TipoSolicitud(db.Model):
    __tablename__ = 'tiposolicitud' #Nombre de la tabla
    idTipoSolicitud = db.Column(db.Integer, primary_key=True, autoincrement=True)
    tipoSolicitud = db.Column(db.String(30),nullable=False)


    def __repr__(self):
        return f'{self.tipoSolicitud},{self.idTipoSolicitud}'
