from app import db

class Oficina(db.Model):
    __tablename__ = 'oficinas' #Nombre de la tabla
    idOficina = db.Column(db.Integer, primary_key=True, autoincrement=True)
    ofiNombre = db.Column(db.String(30),nullable=False)


    def __repr__(self):
        return f'{self.idOficina}'
