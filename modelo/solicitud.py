from app import db
from datetime import datetime


class Solicitud(db.Model):
    __tablename__ = 'solicitudes' #Nombre de la tabla
    idSolicitud = db.Column(db.Integer, primary_key=True, autoincrement=True)
    idTipoSolicitud = db.Column(db.Integer, db.ForeignKey('tiposolicitud.idTipoSolicitud'), nullable=False)
    idOficina = db.Column(db.Integer, db.ForeignKey('oficinas.idOficina'), nullable=False)
    solCodigo = db.Column(db.String(50),nullable=False)
    solFechaRegistro = db.Column(db.DateTime, default=datetime.now(), nullable=False)
    solNombreCiudadano = db.Column(db.String(50), nullable=True)
    solCorreoCiudadano = db.Column(db.String(50), nullable=True)
    solEstado = db.Column(db.String(20),default="Solicitada", nullable=False)
    solDescripcion = db.Column(db.String(300), nullable=False )
    # Necesarios para la relacion
    tipoSolicitud = db.relationship("TipoSolicitud",backref=db.backref('tiposolicitud',lazy=True))
    oficina = db.relationship("Oficina",backref=db.backref('oficina',lazy=True))
    #def __repr__(self):
    #    return f'{self.idSolicitud}'
