from app import db

class Usuario(db.Model):
    __tablename__ = 'usuarios' #Nombre de la tabla
    idUsuario = db.Column(db.Integer, primary_key=True, autoincrement=True)
    idRol = db.Column(db.Integer, db.ForeignKey('rol.idRol'), nullable=False)
    idEmpleado = db.Column(db.Integer, db.ForeignKey('empleados.idEmpleado'), nullable=False)
    usuLogin = db.Column(db.String(50),nullable=False)
    usuContraseña = db.Column(db.String(50),nullable=False)
    usuEstado = db.Column(db.String(20),default="Activo", nullable=False)
    # Necesarios para la relaicion

    rol = db.relationship("Rol",backref=db.backref('rol',lazy=True))
    empleado = db.relationship("Empleado",backref=db.backref('empleados',lazy=True))

    def __repr__(self):
        return f'{self.usuLogin}'
